import 'package:flutter/material.dart';

class AuthScreen extends StatelessWidget {
  const AuthScreen({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
        body: SingleChildScrollView(
      child: Container(
        height: deviceSize.height,
        width: deviceSize.width,
        child: Flexible(
            flex: deviceSize.width > 600 ? 2 : 1,
            child: Column(
              children: [
                Row(children: [
                  Image.asset('assets/images/header_login.png'),
                  Image.asset('assets/images/logo.png', width: 120),
                ]),
                const AuthCard()
              ],
            )),
      ),
    ));
  }
}

class AuthCard extends StatefulWidget {
  const AuthCard({
    Key? key,
  }) : super(key: key);

  @override
  _AuthCardState createState() => _AuthCardState();
}

class _AuthCardState extends State<AuthCard> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  final Map<String?, String?> _authData = {
    'userId': '',
    'password': '',
  };
  var _isLoading = false;
  final _passwordController = TextEditingController();

  void _submit() {
    if (!_formKey.currentState!.validate()) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text("Error!"),
              content:
                  const Text("User ID dan atau Password anda belum diisi!"),
              actions: <Widget>[
                TextButton(
                  onPressed: () => Navigator.pop(context, 'OK'),
                  child: const Text('OK'),
                ),
              ],
            );
          });
    } else {
      _formKey.currentState!.save();
      setState(() {
        _isLoading = true;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text("Success"),
              content: const Text("Login Berhasil"),
              actions: <Widget>[
                TextButton(
                  onPressed: () => Navigator.pop(context, 'OK'),
                  child: const Text('OK'),
                ),
              ],
            );
          });
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return SafeArea(
      child: Center(
        child: Container(
          height: deviceSize.height * 0.75,
          width: deviceSize.width * 0.75,
          alignment: Alignment.center,
          padding: const EdgeInsets.symmetric(horizontal: 12.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          margin: const EdgeInsets.only(bottom: 18.0),
                          child: const Text('Login',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 34))),
                      const Text('Please sign in to continue.',
                          textAlign: TextAlign.left,
                          style: TextStyle(fontSize: 16))
                    ],
                  ),
                  alignment: Alignment.bottomLeft,
                ),
                TextFormField(
                  decoration: InputDecoration(
                      labelText: 'User ID',
                      hintText: 'User ID',
                      hintStyle: TextStyle(
                          fontSize: 12.0,
                          color: Colors.grey[350],
                          fontStyle: FontStyle.italic)),
                  validator: (String? value) {
                    return value != null && value.isEmpty
                        ? 'User ID cannot be empty!'
                        : null;
                  },
                  onSaved: (value) {
                    _authData['userId'] = value;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Password',
                      hintText: 'Password',
                      hintStyle: TextStyle(
                          fontSize: 12.0,
                          color: Colors.grey[350],
                          fontStyle: FontStyle.italic)),
                  obscureText: true,
                  controller: _passwordController,
                  validator: (String? value) {
                    return value != null && value.isEmpty
                        ? 'Password cannot be empty!'
                        : null;
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                if (_isLoading)
                  const CircularProgressIndicator()
                else
                  Container(
                      child: ElevatedButton(
                        child: const Text('LOGIN'),
                        onPressed: _submit,
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30),
                            ),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 40.0, vertical: 16.0),
                            primary: Colors.deepPurple),
                      ),
                      alignment: Alignment.centerRight,
                      margin: const EdgeInsets.only(top: 10.0)),
                const Spacer(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text('Don\'t have an account?'),
                    TextButton(
                      child: const Text('Sign Up'),
                      onPressed: () {},
                      style: TextButton.styleFrom(
                        tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        primary: Colors.deepOrange,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
